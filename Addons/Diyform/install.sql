CREATE TABLE IF NOT EXISTS `sent_diyform`(
	`id` mediumint(8) NOT NULL AUTO_INCREMENT COMMENT '自定义表单ID',
	`title` varchar(40) NOT NULL COMMENT '自定义表单名称',
	`table` varchar(40) NOT NULL COMMENT '自定义表单表名',
	`create_time` varchar(40) NULL DEFAULT NULL COMMENT '创建时间',
	`template_lists` varchar(40) NULL DEFAULT NULL COMMENT '列表模板',
	`template_detail` varchar(40) NULL DEFAULT NULL COMMENT '详情内容也模板',
	`template_add` varchar(40) NULL DEFAULT NULL COMMENT '发布模板',
	`status` int(1) NULL DEFAULT NULL COMMENT '自定义表单状态',
	PRIMARY KEY(`id`)
)ENGINE = MyISAM DEFAULT CHARSET=utf8 COMMENT '自定义表单';
CREATE TABLE IF NOT EXISTS `sent_form_info` (
  `id` mediumint(8) NOT NULL AUTO_INCREMENT COMMENT '自定义表单信息ID',
  `tableid` mediumint(8) NOT NULL COMMENT '自定义表ID',
  `texttitle` varchar(40) NULL DEFAULT NULL COMMENT '文本标题',
  `textname` varchar(40) NULL DEFAULT NULL COMMENT '表单名称',
  `texttype` varchar(40) NULL DEFAULT NULL COMMENT '表单类型',
  `textextra` varchar(100) NULL DEFAULT NULL COMMENT '文本框额外参数',
  `fieldname` varchar(100) NULL DEFAULT NULL COMMENT '字段名称',
  `fieldtype` varchar(100)NULL DEFAULT NULL COMMENT '字段类型',
  `fieldlength` varchar(10) NOT NULL DEFAULT '0' COMMENT '字段长度',
  `fieldisnull` int(1) NOT NULL DEFAULT '0' COMMENT '字段是否为空',
  `fielddefault` varchar(100) NOT NULL DEFAULT '0' COMMENT '字段默认值',
  `fieldcomment` varchar(100) DEFAULT NULL COMMENT '字段描述信息',
  PRIMARY KEY(`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;