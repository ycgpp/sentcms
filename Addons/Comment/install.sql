CREATE TABLE IF NOT EXISTS `sent_comment` (
  `id` mediumint(8) NOT NULL AUTO_INCREMENT COMMENT '评论表ID',
  `uid` mediumint(8) DEFAULT NULL COMMENT '评论用户的Uid',
  `nickname` varchar(40) NOT NULL COMMENT '用户昵称',
  `aid` mediumint(8) NOT NULL COMMENT '文章ID',
  `content` text COMMENT '评论内容',
  `model_id` varchar(40) NOT NULL COMMENT '模型',
  `reply_num` int(10) NOT NULL DEFAULT '0' COMMENT '回复数',
  `status` int(1) DEFAULT '0' COMMENT '状态',
  `ip` varchar(40) NOT NULL DEFAULT '0' COMMENT '发布IP',
  `create_time` varchar(40) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='评论表';

CREATE TABLE IF NOT EXISTS `sent_comment_reply` (
  `id` mediumint(8) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `commentid` mediumint(8) NOT NULL COMMENT '评论ID',
  `uid` mediumint(8) NOT NULL COMMENT '评论用户的uid',
  `nickname` varchar(40) NOT NULL COMMENT '评论用户的昵称',
  `content` text NOT NULL COMMENT '评论内容',
  `ip` varchar(40) NOT NULL DEFAULT '0' COMMENT '最后回复IP',
  `create_time` varchar(40) NOT NULL COMMENT '创建时间',
  `update_time` varchar(40) NOT NULL COMMENT '更新时间',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='评论回复表';
